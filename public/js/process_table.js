Ext.onReady(

function(){

    // create the Data Store
    var processes_store = new Ext.data.Store({
        // load using HTTP
        url: 'webservices/process',

        // the return will be XML, so lets set up a reader
        reader: new Ext.data.XmlReader({
               // records will have an "Item" tag
               record: 'process',
               id: 'id',
               totalRecords: '@total'
           }, [
               // set up the fields mapping into the xml doc
               // The first needs mapping, the others are very basic
               {name: 'name', mapping: 'name'},
               'host', 'status', 'start_time', 'end_time'
           ])
    });

    // create the grid
    var grid = new Ext.grid.GridPanel({
        store: processes_store,
        columns: [
            {header: "Name", dataIndex: 'name', sortable: true},
            {header: "Host", dataIndex: 'host', sortable: true},
            {header: "Status", dataIndex: 'status', sortable: true},
            {header: "Start Time", dataIndex: 'start_time', sortable: true, hidden: true},
            {header: "End Time", dataIndex: 'end_time', sortable: true, hidden: true}
        ],
        renderTo:'process-table',
        autoHeight:true
    });

    processes_store.load();
}

);
