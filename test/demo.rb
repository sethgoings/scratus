$URL = "http://valkyrie:4567/webservices/process"
$HOST = "grape"

require "rest_client"
require "rexml/document"
include REXML

def report(output="Sending", time=3)
  puts
  print "\# #{output}"
  time.times { sleep 1; print '.'}
  puts
end

def ask
  print "--> "
  gets.chomp
end

puts "Welcome to the Scratus demo application!"
puts "This user-operated demo is designed to show the general operation that"
puts "scripts will take in order to interact with the central Scratus server."
puts
puts "I will write information to the screen for you prefixed with a '>' symbol,"
puts "when I'm doing something, I'll prefix the line with a '\#' symbol,"
puts "I'll be waiting for your response whenever I show a '-->' symbol."
puts 
puts "NOTE: Artificial delays have been added to make this demo a little easier"
puts "to work through."
puts

report("Loading")

puts ">>>> INITIALIZE <<<<"
# Get required data to initialize
puts "> To register a process with Scratus, I'll need a script name..."
script = ask

puts
puts "> Thanks. I'm going to register this process now. Scratus will send"
puts "> me back some information which I'll take care of for you for the"
puts "> rest of this demo."

###### INITIALIZE ######
# Execute POST, which will initialize the scripts status record on the Scratus
# server. We get XML back, and all I care about with this
# (at least for this demo) will be taking the first ID and saving it for future
# use.
response = RestClient.post $URL, {:host => $HOST, :name => script}
id = Document.new(response).elements["//process/id"].text.to_i

puts "> Scratus told me that my script id is set as #{id}. This is the id"
puts "> I'll use from now on to make sure I'm updating the right status."

###### UPDATE ######
puts
puts ">>>> UPDATE <<<<"
puts "> Now we can update the processes' status in Scratus as many times"
puts "> as you want. All you need to do is enter in any text you feel like... "
puts "> and press enter. Each line you type will be recorded and sent to "
puts "> Scratus, which will then appear on its gui."
puts "> To finish sending status updates, just type 'finish' (without the"
puts "> quotes of course) and press enter."
while true do
  puts "> Enter a status: "
  status = ask
  break if status.match(/finish/i)
  RestClient.put "#{$URL}/#{id}", {:status => status}
  report("Sending")
  puts "> All good. Our status is now '#{status}' according to Scratus."
  puts "> Send another status? (type text and press enter), or type 'finish'."
end

###### FINALIZE ######
puts
puts ">>>> FINALIZE <<<<"
puts "> Ok. We're almost done. All I need to do is finalize the process"
puts "(make it uneditable from this point forward)."
RestClient.delete "#{$URL}/#{id}"
report("Finalizing")
puts "> All right! I'm done! Feel free to walk through this demo again if you"
puts "> want to try anything else."