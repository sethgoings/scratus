#!/usr/local/bin/ruby

$: << File.expand_path(File.dirname(__FILE__) + "/../")

require 'rest_client'
require "test/unit"

class TestWebServices < Test::Unit::TestCase

  def test_normal_sequences
    register
    update
    get_all
    delete

    register("smarty", "test2")
    update(2, 2)
    get_all

    register("aorta", "ws_test3")
    update(3, 1)
  end

  def test_loaded_system
    20.times do
      register("test_machine", "test_uno_dos_tres")
    end
  end

  def test_invalid_id
    assert_raise RestClient::ResourceNotFound do
      update(-1,1)
    end
  end


  private
  def register(host="marty", name="scratus_restclient_test")
    response = RestClient.post 'http://localhost:4567/webservices/process', {:host => host, :name => name}
  end

  def update(id=1, update_limit=-1)
    array = ["running", "stopped", "running", "almost_finished"]
    
    i = 0
    response = []
    array.each do
      |item|
      break if update_limit == i
      #sleep 1 #comment/uncomment this if you want to make sure the timestamping is working
      response << RestClient.put("http://localhost:4567/webservices/process/#{id}", {:status => item})
      i += 1
    end

    response
  end

  def get_all 
    response = RestClient.get 'http://localhost:4567/webservices/process'
  end

  def delete(id=1)
    response = RestClient.delete "http://localhost:4567/webservices/process/#{id}"
  end

end
