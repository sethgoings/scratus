#!/bin/bash

#----------------------------------------------------------------------------------#
# parseXML is a pseudo parser for the XML in the HISTORY file.  This function was  #
# pull off the web as a sample pseudo parser for XML.  few changes were made to    #
# this suproutine.                                                                 #
#----------------------------------------------------------------------------------#

function parseXML ()
{
   echo ">>> parseXML"
   declare elmentATTN=""           # element attribute name
   declare elmentATTV=""           # element attribute value
   declare elmentNAME=""           # element name value
   declare elmentDUMP=""           # element name dump
   declare parserLINE=""           # contains a single line read from the XML document sent to the parser
   declare parserFLAG="ENABLED"    # can have two mutually exclusive values: ENABLED / DISABLD
   declare parserFILE=""           # file sent to the parser
   declare parserBUFF=""           # parser buffer variable
   declare -i checkpoint=0         # a character counter & pointer for parserLINE

   [[ -z ${1} ]] && return 0
   parsefile=$1
   echo "Parsing file $parsefile"

   while read parserLINE
   do
      parserBUFF=""
      for ((checkpoint=0; checkpoint < ${#parserLINE}; checkpoint++)); do
         case ${parserLINE:$checkpoint:1} in
         $startTAG)
            let "checkpoint++"; elmentNAME=""; parserFLAG="ENABLED"
            if [ "$parserBUFF" != '' ]; then
               xmlSTAT[xINDEX]="#"
               xmlDATA[xINDEX]="$parserBUFF"
               let "xINDEX++"
               parserBUFF=""
            fi
            until [ "${parserLINE:$checkpoint:1}" = ' ' ] || \
                  [ "${parserLINE:$checkpoint:1}" = '>' ] || \
                  [ $checkpoint = ${#parserLINE} ]; do
               elmentNAME=$elmentNAME${parserLINE:$checkpoint:1}
               let "checkpoint++"
            done
            if [ "${elmentNAME:0:1}" = "$slashTAG" ]; then
               xmlDATA[xINDEX]="${elmentNAME#*/}"
               xmlSTAT[xINDEX]="$closeTAG"
               let "xINDEX++"
               parserBUFF=""
            else
               xmlDATA[xINDEX]="$elmentNAME"
               xmlSTAT[xINDEX]="$startTAG"
               let "xINDEX++"; parserBUFF=""
               case ${parserLINE:$checkpoint:1} in
                  $whiteTAG)	elmentDUMP="$elmentNAME" ;;
                  '')		elmentDUMP="$elmentNAME" ;;
               esac
            fi
         ;;
         $slashTAG)
            case $parserFLAG in
               ENABLED)
                  if [ "${parserLINE:$((checkpoint + 1)):1}" = "$closeTAG" ]; then
                     xmlSTAT[xINDEX]="$closeTAG"
                     xmlDATA[xINDEX]="$elmentDUMP"
                     let "xINDEX++"
                     elmentDUMP=""; parserBUFF=""; let "checkpoint+=2"
                  fi
               ;;
               DISABLD)
               ;;
            esac
         ;;
         $quoteTAG)
            case $parserFLAG in
               ENABLED)
                  let "checkpoint++"; elmentATTV=""; parserBUFF=""
                  until [ "${parserLINE:$checkpoint:1}" = '"' ]; do
                     elmentATTV=$elmentATTV${parserLINE:$checkpoint:1}
                     let "checkpoint++"
                  done
                  let "checkpoint++"; xmlDATA[xINDEX]="$elmentATTV"; let "xINDEX++"
                  case ${parserLINE:$checkpoint:1} in
                     $closeTAG)	if [ "${parserLINE:$((checkpoint + 1)):1}" = '<' ] ; then let "checkpoint-=1"; fi ;;
                     $slashTAG)	let "checkpoint-=1" ;;
                  esac
               ;;
               DISABLD)
               ;;
            esac		
            ;;
         $equalTAG)
            case $parserFLAG in
               ENABLED)
                  elmentATTN=""
                  if [ ${parserLINE:$((checkpoint + 1)):1} = '"' ]; then
                     elmentATTN=${parserBUFF##$equalTAG}
                     elmentATTN=${elmentATTN##*$whiteTAG}
                     xmlSTAT[xINDEX]="$elmentATTN"
                  fi
               ;;
               DISABLD)
               ;;
            esac
         ;;
         esac
   
         case $((checkpoint + 1)) in
            ${#parserLINE})
               parserBUFF=$parserBUFF${parserLINE:$checkpoint:1}
               if [ "${parserLINE:$checkpoint:1}" != "$closeTAG" ] ; then
                  xmlSTAT[xINDEX]="#"
                  xmlDATA[xINDEX]="$parserBUFF"
                  let "xINDEX++"
               fi
            ;;
            *)
               case ${parserLINE:$checkpoint:1} in
                  $closeTAG)	parserBUFF=""; parserFLAG="DISABLD";;
                  *)		parserBUFF=$parserBUFF${parserLINE:$checkpoint:1};;
               esac
            ;;
         esac
      done
   done < "$parsefile"
   echo "<<< parseXML"
}

#----------------------------------------------------------------------------------#
# getLevels get's the levels out of the parsed history file, it will updated the   #
# levellist array, or return FULLBUILD, meaning get all committed files.           #
#----------------------------------------------------------------------------------#

function getLevels ()
{
   echo ">>> getLevels"
   declare -r inversion="${1}"    # input version
   echo "getting Levels for version $inversino"

   declare -i doall=0             # a simple counter variable

   parseXML $histfile             # parse the XML

   unset foundvtag
   unset inlevels
   unset inpatch
   for (( doall=0; doall < $xINDEX; doall++ )); do
      if [[ ${xmlSTAT[doall]} = "property" ]] && [[ ${xmlDATA[doall]} = "event" ]]; then
         ((tmpidx=doall+2))
         [[ $tmpidx -ge $xINDEX ]] && break
         [[ ${xmlSTAT[tmpidx]} = "#" ]] && [[ ${xmlDATA[tmpidx]} = "patching" ]] && inpatch="true"
      fi

      if [[ ${xmlSTAT[doall]} = "property" ]] && [[ ${xmlDATA[doall]} = "version" ]]; then
         unset foundversion
         ((tmpidx=doall+2))
         [[ $tmpidx -ge $xINDEX ]] && break
         xmlversion=${xmlDATA[tmpidx]}
         [[ $inversion = $xmlversion ]] && foundversion="true"
	 [[ -n $foundversion ]] && [[ -z $inpatch ]] && return $FULLBUILD
      fi
      
      if [[ -n $foundversion ]] && [[ ${xmlSTAT[doall]} = "property" ]] && [[ ${xmlDATA[doall]} = "levels" ]]; then
         ((tmpidx=doall+2))
	 [[ $tmpidx -ge $xINDEX ]] && break
         [[ ${xmlSTAT[tmpidx]} = "class" ]] && [[ ${xmlDATA[tmpidx]} = "java.util.ArrayList" ]] && inlevels="true"
      fi

      if [[ -n $inlevels ]] && [[ ${xmlSTAT[doall]} = $closeTAG ]] && [[ ${xmlDATA[doall]} = "object" ]]; then
         unset foundversion
	 unset inlevels
      fi

      if [[ -n $inlevels ]] && [[ ${xmlSTAT[doall]} = "method" ]] && [[ ${xmlDATA[doall]} = "add" ]]; then
         ((tmpidx=doall+2))
	 [[ $tmpidx -ge $xINDEX ]] && break
	 levellist[lvlidx]=${xmlDATA[tmpidx]}
	 ((lvlidx++))
      fi
   done
   echo "<<< getLevels"
}
