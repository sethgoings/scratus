#!/usr/local/bin/ruby

$: << File.expand_path(File.dirname(__FILE__))

require 'sinatra'
require 'sequel'
require 'libxml'
require 'erb'
require 'redcloth'

db = Sequel.sqlite("db/processes.db")

db.create_table? :processes do
  primary_key :id
  String :name
  String :host
  String :status
  String :start_time
  String :end_time
  String :comments
end

class Processes < Sequel::Model
end

helpers do

  # Downcases all entries in params
  def prep_insert(params)
    params.each do
      |item|
      item.last.downcase!
    end
  end

  # Retrieves all data on process.
  # Acceptable options include:
  # * :id (any valid ID in system, otherwise you'll get a blank resultset)
  # * :name (see :id, will search through DB and return matching names)
  # * nothing, which will return all Processes
  def get_process_details_xml(params={})
    headers 'Content-Type' => 'text/xml'

    procs = nil

    if(!params.empty?)
      filters = {}
      params.each do
        |item|
        # The check for HTTP vars starting with _ is to eliminate passing of vars from ExtJS to the DB
        filters[item.first.to_sym] = item.last unless /^_/.match(item.first)
      end
        procs = Processes.filter(filters)
    else
      procs = Processes.all
    end

    doc = LibXML::XML::Document.new
    doc.root = LibXML::XML::Node.new('resultset')
    root = doc.root

    procs.each do
      |proc|
      root << result = LibXML::XML::Node.new('process')
      proc.each do
        |col|
        result << element = LibXML::XML::Node.new(col.first) 
        element << col.last.to_s
      end
    end

    return doc.to_s
  end

end

### HUMAN VIEWS ###

get '/' do
  erb :index
end

get '/help' do
  textile :README
end

### WEBSERVICES ###

# Register a process using this method. Must supply:
# * :host
# * :name
# Will return XML with created record.
post '/webservices/process' do
  halt 400, "Incorrect REST call." if params[:host].nil? || params[:name].nil?
  
  prep_insert(params)
  proc = Processes.insert(
                      :host => params[:host], 
                      :name => params[:name], 
                      :status=> "initialized", 
                      :start_time => Time.now.utc.to_s, 
                      :comments => params[:comments]
                    )
  get_process_details_xml(params)
end

# Update an existing process using this method. Must supply:
# :status
# Will return XML with updated record.
put '/webservices/process/:id' do
  halt 400, "Incorrect REST call." if params[:status].nil?

  prep_insert(params)
  proc = Processes.filter(:id => params[:id]).first
  # It's forbidden to set status = complete or update the status when the process has already reported complete.
  # use the DELETE REST call if you'd like to complete
  halt 404, "Process with id = #{params[:id]} was not found." if proc.nil?
  halt 403, "Use the DELETE REST call on a method if you'd like to complete a process." if proc[:status] == "complete" || params[:status] == "complete"
  proc.update(:status => params[:status])
  get_process_details_xml(params)
end

# Returns XML resultset of all process in system.
get '/webservices/process' do
  get_process_details_xml(params)
end

# Returns XML resultset with process identified by :id
get '/webservices/process/:id' do
  get_process_details_xml(params)
end

# Returns XML resultset with process identified by :name
get '/webservices/process' do
  get_process_details_xml(params)
end

# Finalizes process with id matching :id
delete '/webservices/process/:id' do
  proc = Processes.filter(:id => params[:id]).first
  proc.update(:status => "complete", :end_time => Time.now.utc.to_s)
  get_process_details_xml(params)
end
